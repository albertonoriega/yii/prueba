<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario $model */
/** @var ActiveForm $form */
?>
<div class="site-tres">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nombre')->textInput(['placeholder'=> 'Introduce nombre']) ?>
        <?= $form->field($model, 'apellidos')->textInput(['placeholder'=> 'Introduce nombre']) ?>
        <?= $form->field($model, 'edad')->input('number') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
