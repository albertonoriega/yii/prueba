<?php

use app\models\Ciudades;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ciudades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ciudades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'habitantes',
            //'escudo',
            [
                "attribute" => "escudo",
                "format"=> "raw",
                "value" => function ($model){
                    if (isset($model->escudo) && $model->escudo!=""){
                        return Html::img("@web/imgs/$model->id/$model->escudo",
                            ["class" => "img-thumbnail col-3 d-block m-auto"],
                            
                        );
                    }
                    $nombre="anonimo.jpg";
                    return Html::img("@web/imgs/$nombre",
                        ["class" => "img-thumbnail col-3 d-block m-auto"],
                        );
                }
            ],
            //'mapa',
            [
            'label' => 'Mapa',
            'format' => 'raw',
            'value' => function ($dato) {
                return Html::a(
                        "Ir al mapa", 
                        $dato->mapa,
                        ["class" => "btn btn-light text-danger m-3"]
                );
            }
        ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ciudades $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nombre' => $model->nombre]);
                 }
            ],
        ],
    ]); ?>


</div>
