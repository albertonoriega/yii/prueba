<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Ciudades $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ciudades-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'nombre' => $model->nombre], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'nombre' => $model->nombre], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'habitantes',
            //'escudo',
            [
                "attribute" => "escudo",
                "format"=> "raw",
                "value" => function ($model){
                    if (isset($model->escudo) && $model->escudo!=""){
                        return Html::img("@web/imgs/$model->id/$model->escudo",
                            ["class" => "img-thumbnail col-3 d-block m-auto"],
                            
                        );
                    }
                    $nombre="anonimo.jpg";
                    return Html::img("@web/imgs/$nombre",
                        ["class" => "img-thumbnail col-3 d-block m-auto"],
                        );
                }
            ],
            //'mapa',
                    [
            'label' => 'Mapa',
            'format' => 'raw',
            'value' => function ($dato) {
                return Html::a(
                        "Ir al mapa", 
                        $dato->mapa,
                        ["class" => "btn btn-light text-danger m-3"]
                );
            }
        ],
        ],
    ]) ?>

</div>
