<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\models;

/**
 * Description of Formulario
 *
 * @author alberto
 */
class Formulario extends \yii\base\Model {
    
    public ?string $nombre=null; 
    public ?string $apellidos=null;
    public ?int $edad=null; 
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            
        ];
    }
    
    public function rules(): array {
        return [
            [['nombre','apellidos','edad'],'required'],
            [['nombre','apellidos'], 'string', 'max' => 50],
            [['edad'], 'number'],
        ];
    }
}
